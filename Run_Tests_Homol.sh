#!/usr/bin/env bash

# check for errors and exit
set -e
set -x

# config overridable args
collection='collection/Neon1.postman_collection.json'
environment='environments/DEV.postman_environment.json'
global=''
webhook='https://hooks.slack.com/services/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'
additional=''

# global vars
newman_required_ver='4.1.0'
node_required_ver='4.0.0'
newman_args='-r cli,html --reporter-html-template utils/reporter.hbs --reporter-html-export reporter_API.html'
verbose=0

# prepend newman args to commands
 prepend_newman_args () {

    if [ -n "$global" ] ; then
        global="-g $global"
    fi

    if [ -n "$environment" ] ; then
        env="-e $environment"
    fi
}

  # process the script
    main () {

    # prepend newman arguments to vars
      prepend_newman_args

      # call newman
        local output=$(newman run $collection $env $newman_args)

      #failed the tests
        if echo $output | grep -E 'Success:true|Error'; then

          # post to slack success
          curl -X POST --data-urlencode "payload={
                                                   \"channel\": \"#env-healthcheck\",
                                                   \"username\": \"TESTES API NEON - HOMOLOCACÃO\",
                                                   \"text\": \"<!here> \nOs Testes Falharam - Bora arrumar =/\",
                                                   \"attachments\": [
                                                     {
                                                       \"fallback\": \"Test Results : https://jenkins.devneon.com.br:1443/job/${JOB_NAME}/${BUILD_NUMBER}/artifact/reporter_API.html\",
                                                       \"text\": \"<https://jenkins.devneon.com.br:1443/job/${JOB_NAME}/${BUILD_NUMBER}/artifact/reporter_API.html|Test Results>\",
                                                       \"fields\": [
                                                       ],
                                                       \"color\": \"#FF0000\"
                                                     }
                                                   ]
                                                 }
          " $webhook
          exit 1
        else
          # post to slack
          curl -X POST --data-urlencode "payload={
                                                   \"channel\": \"#env-healthcheck\",
                                                   \"username\": \"TESTES API NEON - HOMOLOCACÃO\",
                                                   \"text\": \"Os Testes Passaram - Uhullll =)\",
                                                   \"attachments\": [
                                                     {
                                                       \"fallback\": \"Test Results : https://jenkins.devneon.com.br:1443/job/${JOB_NAME}/${BUILD_NUMBER}/artifact/reporter_API.html\",
                                                       \"text\": \"<https://jenkins.devneon.com.br:1443/job/${JOB_NAME}/${BUILD_NUMBER}/artifact/reporter_API.html|Test Results>\",
                                                       \"fields\": [
                                                       ],
                                                       \"color\": \"#008000\"
                                                     }
                                                   ]
                                                 }
          " $webhook
        fi
}

  # initialize script
  main "$@"
